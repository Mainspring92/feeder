#!/usr/bin/python3
import urllib.request, json, re, sys, argparse, getopt
from urllib.parse import urlparse
import modules.collection
import modules.UI
import os
#Arguments for input in CLI
def _parser():
    parser  = argparse.ArgumentParser()
    parser.add_argument('-u', '--url', help="Hostname of ayfie server", type=str)
    parser.add_argument('-i', '--id', help="Id of collection", type=str)
    parser.add_argument('-p', '--port', help="Port of hostname", nargs="?", const="80", type=str)
    parser.add_argument('-f', '--filepath', help="Path to the file", nargs="?", const="NULL", type=str)

    args = parser.parse_args()

    if args.url and args.id:
        if urlparse(args.url).scheme =='http' or urlparse(args.url).scheme == 'https':
            # _col_validate = _collection_validate(args.url, args.id, args.port)
            _usr_menu = modules.UI._user_menu(args.url, args.id, args.port, args.filepath)
        else:
            url = "http://" + str(args.url)
            # _col_validate = _collection_validate(url, args.id, args.port)
            _usr_menu = modules.UI._user_menu(url, args.id, args.port, args.filepath)
    else:
        print(parser.print_help())
        sys.exit()

if __name__ == '__main__':
    os.system('clear')
    _parser()
    UI._user_menu(url, id, port, filepath)
