#!/usr/bin/python3
import urllib.request, json, re, sys, argparse, getopt
from urllib.parse import urlparse
import modules.collection
import modules.doc
import libs.lib
import os
#Menu for user
def _user_menu(url, id, port, filepath):
    ans=True
    while ans:
        print("\n-------------------------------------------")
        print("USER INPUT PARAMETERS")
        print("-------------------------------------------")
        print("\nHostname: %s" % url)
        if port == None:
            port = "80"
        print("Port: %s" % port)
        print("Collection id: {0} => State: {1}".format(id, modules.collection.collections._collection_state(url, id, port)))
        if filepath == None:
            libs.lib.prYellow("[warning]: No file set. Please indicate the documents for feeding.")
        else:
          print("Filepath: %s" % filepath)
        print("\n-------------------------------------------")
        print("MENU")
        print("-------------------------------------------")
        print("1. Start program")
        print("2. Delete collection")
        print("3. List collections")
        print("4. JSON Dump")
        print("5. Exit/Quit\n")
        if modules.collection.collections._collection_state(url, id, port) == "COMMITTED":
          print("\n-------------------------------------------")
          print("ADDITIONALL FUNCTIONS")
          print("-------------------------------------------")
          print("6. Process collection %s\n\n" % id)
        ans=input("What would you like to do? ")
        if ans=="6":
          os.system('clear')
          url = modules.collection.collections.validateUrl(url, id, port)
          modules.collection.collections._collection_process(url)
        if ans=="2":
          os.system('clear')
          modules.collection.collections._delete_collection(url, id, port)
        elif ans == "3":
            modules.collection.collections._list_collection(url, id, port)
        elif ans=="4":
          os.system('clear')
          print("\nDumping JSON ...")
          if filepath == None or filepath == "NULL":
              libs.lib.prYellow("\n[warning]: No file detected. Please choose file.\n")
              input("\n\nPress Enter to continue...")
              os.system('clear')
          else:
              NewItem = modules.doc.documents()
              NewItem._return_ext(filepath)
              libs.lib.prGreen("\n[success]: File has been created\n")
              sys.exit()
        elif ans=="1":
          os.system('clear')
          print("\n-------------------------------------------")
          print("PROGRAM RUNNING ....")
          print("-------------------------------------------")
          modules.collection.collections._collection_validate(url, id, port, filepath)
        elif ans=="5":
          sys.exit()
        elif ans !="":
          print("\nNot Valid Choice Try again")
