#!/usr/bin/python3
import os, json, zipfile, ntpath

data = {}
data["documents"] =[]
class documents:
    def _return_ext(self, filepath):
        # self.ext = ext
        self.filepath = filepath
        ext = os.path.splitext(filepath)[1]
        id = ntpath.basename(filepath)
        if os.path.exists(filepath):
            if ext == ".txt":
                with open (filepath) as outfile:
                    for line in outfile:
                        line = line.replace(" ","\r")
                        data_json ={"id": os.path.splitext(id)[0],"fields":{"content":line}}
                    data["documents"].append(data_json)
                    self._documents_json_dump(data)
            if os.path.isdir(filepath):
                if not os.listdir(filepath):
                    print("Empty Dir")
                elif os.listdir(filepath):
                    _files = (os.listdir(filepath))
                    for file in _files:
                        if os.path.splitext(file)[1] == ".txt":
                            with open (filepath + "/" + file) as outfile:
                                for line in outfile:
                                    line = line.replace(" ","\r")
                                    data_json ={"id": os.path.splitext(file)[0],"fields":{"content":line}}
                                data["documents"].append(data_json)
                                self._documents_json_dump(data)
                        else:
                            print("Unknown format file")
            if ext == ".rar":
                self._rar_ext(filepath)
            elif ext == ".zip":
                self._zip_ext(filepath)
        else:
            print("Path not exist")

    def _rar_ext(self, filepath):
        print("RAR FLE")

    def _json_ext(filepath):
        return filepath
        _collection_batch(url, filepath)


    def _zip_ext(self, filepath):
        zfile = zipfile.ZipFile(filepath)
        for file in zfile.namelist():
            single_file = ntpath.basename(file)
            file_check = os.path.splitext(single_file)[1]
            counter = 0
            if file_check == ".txt":
                with open (file) as outfile:
                    for line in outfile:
                        line = line.replace(" ","\r")
                        data_json ={"id": os.path.splitext(single_file)[0],"fields":{"content":line}}
                    data["documents"].append(data_json)
                    self._documents_json_dump(data)


    def _documents_json_dump(self,data):
        self.data = data
        print(data)
        with open("data.json",'w') as outfile:
            outfile.write(json.dumps(data))
