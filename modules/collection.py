#!/usr/bin/python3
import urllib.request, json, re, sys, argparse, getopt, os
from urllib.parse import urlparse
import libs.lib
import modules.doc
import requests
import time

path ="/ayfie/v1/collections"

class collections:

    def _collection_commit(url):
        data = {
        "collectionState":"COMMIT"
        }
        params = json.dumps(data).encode('utf-8')
        requests.put(url, data=params, headers={'Content-Type': 'application/json'})
        print("Committing Collection ...")
        timeStart = time.time()
        while collections._checkState(url) != "COMMITTED":
            time.sleep(5)
        timeEnd = time.time()
        total = timeEnd - timeStart
        libs.lib.prGreen("\n[success] in %.2f s" % total)
        while True:
            choose = input("Do you want to PROCESS the collection ? [Y]/[N]: ")
            if choose.strip() == "Y" or choose.strip() == "y":
                collections._collection_process(url)
            else:
                break
    def _checkState(url):
        list = urllib.request.urlopen(url)
        data = json.loads(list.read().decode('utf-8'))
        data_json = data["collectionState"]
        return data_json

    def _collection_process(url):
        data = {
        "collectionState":"PROCESS"
        }
        params = json.dumps(data).encode('utf-8')
        req = requests.put(url, data=params, headers={'Content-Type': 'application/json'})
        print("Processing Collection ...")
        timeStart = time.time()
        while collections._checkState(url) != "PROCESSED":
            time.sleep(60)
        timeEnd = time.time()
        total = timeEnd - timeStart
        libs.lib.prGreen("\n[success] in %.2f s" % total)

    def _collection_batch(url, filepath):
        url = url + "/" + "batches"
        if (os.path.splitext(filepath)[1]) == ".json":
            print("Feeding Documents ...")
            headers = {'Content-Type' : 'application/json'}
            requests.post(url, data=open(filepath, 'rb'), headers=headers)
            libs.lib.prGreen("\n[success]")
        else:
            output = "data.json"
            with open(output,'r') as outfile:
                req = urllib.request.Request(url, data=outfile, headers={'Content-Type': 'application/json'})
                response = urllib.request.urlopen(req)
                print("Feeding files ...")
                libs.lib.prGreen("\n[success]")

    def _collection_validate(url, id, port, filepath):
        url_new = collections.validateUrl(url, id, port)
        try:
            req = urllib.request.Request(url_new)
            urllib.request.urlopen(req)
            libs.lib.prBlue("\n[info]: Collection '%s' already exist\n" % id)

            while True:
                choose = input("Would you like to continue ? [Y/N] ")

                if choose.strip() == "N" or choose.strip() == "n":
                    sys.exit()
                else:
                    collections._collection_process(url_new)
                    # collections._collection_batch(url_new, filepath)
                    # if collections._collection_state(url, id, port) == "EMPTY":
                    #     collections._collection_batch(url_new, filepath)
                    # else:
                    #     if collections._collection_state(url, id, port)  == "IMPORTING":
                    #         collections._collection_commit(url_new)
                    #     else:
                    #         collections._collection_process(url_new)
                    sys.exit()
        except urllib.error.HTTPError as e:

            while True:
                libs.lib.prBlue("\n[info] Collection not exist\n")
                choose = input("Do you want to create these collection? [Y]/[N]: ")
                if choose.strip() == "Y" or choose.strip() == "y":
                    collections._create_collection(url_new, id)
                    NewItem = modules.doc.documents()
                    NewItem._return_ext(filepath)
                    collections._collection_batch(url_new, filepath)
                    collections._collection_commit(url_new)
                    sys.exit()
                elif choose.strip() == "N" or choose.strip() == "n":
                    libs.lib.prBlue("\n[info]: Collection will be not create.")
                    print("\nClosing the program ...\n")
                    sys.exit()

#Creating collection if not exist
    def _create_collection(url, id):
        os.system('clear')
        url = url.replace(id, '')
        data= {
            'name':id,
            'id':id
        }
        print("Creating collection %s ..." % id)
        params = json.dumps(data).encode('utf-8')
        req = urllib.request.Request(url, data=params, headers={'Content-Type': 'application/json'})
        response = urllib.request.urlopen(req)
        libs.lib.prGreen("\n[success]")
        print("Collection " + id + " " + str(response.reason))
        print("\n")

#DELETING COLLECTION
    def _delete_collection(url, id, port):
        o = urlparse(url)
        url = o.scheme + "://" + o.netloc + ":" + port + path + "/" + id
        try:
            response = urllib.request.urlopen(url)
            delete  = requests.delete(url)
            print("Collection ID: %s" % id)
            libs.lib.prGreen("\n[success] Collection %s deleted\n" %id)
            input("\n\nPress Enter to continue...")
            os.system('clear')
        except Exception as e:
            libs.lib.prRed("\n[error]: Connection Refused. Please check the hostname or port. Collection could not exist\n")
            input("\n\nPress Enter to continue...")
            os.system('clear')

    def _collection_state(url, id ,port):
            o = urlparse(url)
            url = o.scheme + "://" + o.netloc + ":" + port + path + "/" + id
            try:
                list = urllib.request.urlopen(url)
                data = json.loads(list.read().decode('utf-8'))
                data_json = data["collectionState"]
                return(data_json)
            except Exception as e:
                return("Collection Not Exist")
#LIST COLLECTION
    def _list_collection(url, id, port):
        os.system('clear')
        o = urlparse(url)
        url = o.scheme + "://" + o.netloc + ":" + port + path
        list = urllib.request.urlopen(url)
        data = json.loads(list.read().decode('utf-8'))
        data_json = data["_embedded"]["collections"]
        print("----------------------------------------------------\n")
        print("Available Collections: %s\n" % url)
        print("----------------------------------------------------\n")
        counter=0
        for item in data_json:
            counter+=1
            print("%s: Collection ID : " % str(counter) + item["id"] + " => State: " + item["collectionState"])
        input("\n\nPress Enter to continue...")
        os.system('clear')

    def validateUrl(url, id, port):
        if port != "80":
            o = urlparse(url)
            url_new = o.scheme + "://" + o.netloc + ":" + port + path + "/" + id
        else:
            try:
                with urllib.request.urlopen(url_new) as response:
                    html = response.read()
            except  urllib.error.URLError as e:
                libs.lib.prRed("\n[error]: Connection Refused. Please check the hostname or port\n")
                sys.exit()
        return url_new